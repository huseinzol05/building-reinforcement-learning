import numpy as np
from ple import PLE
import random
from ple.games.flappybird import FlappyBird
from collections import deque
from evolution_strategy import *

class Agent:

    EXPLORATION = 100000
    REPLAY_MEMORY_SIZE = 50000
    BATCH = 32
    POPULATION_SIZE = 15
    SIGMA = 0.1
    LEARNING_RATE = 0.03
    INITIAL_EXPLORATION = 0.0
    FINAL_EXPLORATION = 0.0
    FEATURES = 8
    GAMMA = 0.99
    INITIAL_MEMORY = np.zeros((1, FEATURES))
    MEMORIES = deque()
    FIRST_TIME = True
    # based on documentation, features got 8 dimensions

    def __init__(self, model, screen=False, forcefps=True):
        self.model = model
        self.game = FlappyBird(pipe_gap=125)
        self.env = PLE(self.game, fps=30, display_screen=screen, force_fps=forcefps)
        self.env.init()
        self.env.getGameState = self.game.getGameState
        self.es = Deep_Evolution_Strategy(self.model.get_weights(), self.get_reward, self.POPULATION_SIZE, self.SIGMA, self.LEARNING_RATE)
        self.exploration = self.INITIAL_EXPLORATION

    def get_predicted_action(self, sequence):
        prediction = self.model.predict(np.array(sequence))[0]
        return np.argmax(prediction)

    def get_state(self):
        state = self.env.getGameState()
        return np.array(list(state.values()))

    def get_reward(self, weights):
        self.model.weights = weights
        self.env.reset_game()
        if self.FIRST_TIME:
            state = self.get_state()
            self.INITIAL_MEMORY[0,:] = state
            self.FIRST_TIME = False
        dead = False
        while len(self.MEMORIES) < self.BATCH or not dead:
            if self.exploration > self.FINAL_EXPLORATION:
                self.exploration -= (self.INITIAL_EXPLORATION - self.FINAL_EXPLORATION) / self.EXPLORATION
            if random.random() < self.exploration:
                action = random.choice([0, 1])
            else:
                action = self.get_predicted_action(self.INITIAL_MEMORY)
            real_action = 119 if action == 1 else None
            reward = self.env.act(real_action)
            dead = self.env.game_over()
            future_state = self.get_state()
            self.MEMORIES.append((self.INITIAL_MEMORY, action, reward, future_state, dead))
            if len(self.MEMORIES) > self.REPLAY_MEMORY_SIZE:
                self.MEMORIES.popleft()
            self.INITIAL_MEMORY[0,:] = future_state
        minibatch = random.sample(self.MEMORIES, self.BATCH)
        states_batch = [d[0][0,:] for d in minibatch]
        action_batch = [d[1] for d in minibatch]
        reward_batch = [d[2] for d in minibatch]
        future_states_batch = [d[3] for d in minibatch]
        y_batch = []
        future_actions = self.model.predict(np.array(future_states_batch))
        initial_actions = self.model.predict(np.array(states_batch))
        onehot_actions = np.zeros((self.BATCH,2))
        for i in range(len(minibatch)):
            onehot_actions[i, action_batch[i]] = 1.0
            if minibatch[i][4]:
                y_batch.append(reward_batch[i])
            else:
                y_batch.append(reward_batch[i] + self.GAMMA * np.max(future_actions[i,:]))
        return -np.mean(np.square(y_batch - np.sum((initial_actions * onehot_actions),axis=1)))

    def fit(self, iterations, checkpoint):
        self.es.train(iterations,print_every=checkpoint)

    def play(self, debug=False, not_realtime=False):
        total_reward = 0.0
        self.env.force_fps = not_realtime
        self.env.reset_game()
        state = self.get_state()
        self.INITIAL_MEMORY[0,:] = state
        done = False
        while not done:
            action = self.get_predicted_action(self.INITIAL_MEMORY)
            if action == 1:
                real_action = 119
                if debug:
                    print('eh, jump!', 'total rewards:', total_reward)
            else:
                real_action = None
            total_reward += self.env.act(real_action)
            state = self.get_state()
            self.INITIAL_MEMORY[0,:] = state
            done = self.env.game_over()
        print('game over!')
